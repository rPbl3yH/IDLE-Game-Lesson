@echo off

set UNITY_PATH="C:\Program Files\Unity\Hub\Editor\2021.3.7f1\Editor\Unity.exe"
set PROJECT_PATH="E:\Unity\Projects\Continuous_Integration\IDLE-Game-Lesson"

%UNITY_PATH% -projectPath -batchmode %PROJECT_PATH% -runTests -testResults ./unit-tests.xml -quit
echo Building Unity project...
%UNITY_PATH% -projectPath %PROJECT_PATH% -logfile E:\Logs\CiCDBuild.log -username %UNITY_USERNAME% -password %UNITY_PASSWORD% -executeMethod CustomBuilder.Build -quit

if %errorlevel% neq 0 (
    echo Build failed with error code %errorlevel%.
    exit /b %errorlevel%
)

echo Build completed.