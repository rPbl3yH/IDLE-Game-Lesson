using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CustomBuilder : MonoBehaviour
{
    public static void Build()
    {
        // Получаем все сцены, добавленные в настройки сборки
        var scenes = EditorBuildSettings.scenes;

        // Создаем массив путей к сценам
        var scenePaths = new string[scenes.Length];
        for (int i = 0; i < scenes.Length; i++)
        {
            scenePaths[i] = scenes[i].path;
        }

        var currentDirectory = Directory.GetCurrentDirectory();
        
        // Задаем параметры сборки
        var buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = scenePaths, // Используем все сцены из настроек сборки
            locationPathName = $"{currentDirectory}/Builds/Android/MyIdleGame.apk", // Путь к выходному файлу сборки
            target = BuildTarget.Android, // Целевая платформа
            options = BuildOptions.None
        };

        // Выполняем сборку
        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}
